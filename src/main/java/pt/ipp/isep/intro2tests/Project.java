package pt.ipp.isep.intro2tests;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Project {

    private String _title;
    private LocalDateTime _startDate;
    private LocalDateTime _endDate;

    private List<Resource> _resources = new ArrayList<Resource>();

    public Project( String title, LocalDateTime startDate, LocalDateTime endDate ) throws IllegalArgumentException {

        if( title != null && !title.isEmpty() && !title.isBlank() &&
            startDate != null && endDate != null & startDate.isBefore( endDate ) ) {

            this._title = title;
            this._startDate = startDate;
            this._endDate = endDate;
        }
        else
            throw new IllegalArgumentException("Erros nos valores dos parâmetros.");
    }

    public boolean hasTitle( String title ) {
        return this._title == title ? true : false;
    }

    public boolean addResource( Resource r ) {

        return this._resources.add( r );
    }
    
    public long maxCostInPeriod( LocalDateTime startPeriod, LocalDateTime endPeriod ) throws Exception {

        if( !startPeriod.isBefore(endPeriod) )
            throw new Exception("Período inválido");

            long maxCost = 0;
        for (Resource resource : _resources) {
            maxCost += resource.maximumCostInPeriod(startPeriod, endPeriod);
        }

        return maxCost;
    }

}
