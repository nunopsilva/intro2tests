package pt.ipp.isep.intro2tests;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class StoreProjectReeng {

    private ProjectFactory _projectFactory;

    private List<Project> _projects = new ArrayList<Project>();

    public StoreProjectReeng( ProjectFactory projectFactory ) {
        this._projectFactory = projectFactory;
    }

    public boolean createAndAddProject( String title, LocalDateTime startDate, LocalDateTime endDate) throws IllegalArgumentException {

        if( getByTitle(title) != null )
            throw new IllegalArgumentException("Título já existente.");

        Project newProject = this._projectFactory.createProject( title, startDate, endDate );

        return this._projects.add( newProject );
    }

    public Project getByTitle( String title ) {
        for (Project project : _projects) {
            if( project.hasTitle( title ) )
                return project;
        }

        return null;
    }
}
