package pt.ipp.isep.intro2tests;

public class User {
    private String _name;

    public User( String name ) throws IllegalArgumentException {
        if( name != null && !name.isBlank() && !name.isEmpty() )
            this._name = name;
        else
            throw new IllegalArgumentException("Valor de parâmetro 'name' deve ser uma string não vazia.");
    }

    public boolean equals( Object obj ) {
        if( this == obj )
            return true;

        if( obj instanceof User ) {
            User user = (User) obj;

            if( this._name.equals( user._name ) )
                return true;
        }

        return false;
    }

    public String getNome() {
        return this._name;
    }
}
