package pt.ipp.isep.intro2tests;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class StoreProjectOriginal {

    private List<Project> _projects = new ArrayList<Project>();

    public boolean createAndAddProject( String title, LocalDateTime startDate, LocalDateTime endDate) throws IllegalArgumentException {
        
        // difficuldade para isolar construtor
        Project newProject = new Project( title, startDate, endDate );

        return this._projects.add( newProject );
    }
}
