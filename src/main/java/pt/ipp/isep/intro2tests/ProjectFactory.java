package pt.ipp.isep.intro2tests;

import java.time.LocalDateTime;

public interface ProjectFactory {

    public Project createProject( String title, LocalDateTime startDate, LocalDateTime endDate ) throws IllegalArgumentException;

}
