package pt.ipp.isep.intro2tests;

import java.time.Duration;
import java.time.LocalDateTime;

public class Resource {
    private User _user;
    private LocalDateTime _startDate;
    private LocalDateTime _endDate;
    private long _costPerHour;
    private long _workingHoursPerDay;

    public Resource(User user, LocalDateTime startDate, LocalDateTime endDate, long costPerHour, long workingHoursPerDay ) throws IllegalArgumentException {

        if( user != null &
            startDate != null && endDate != null & startDate.isBefore( endDate ) &
            costPerHour > 0 &
            workingHoursPerDay > 0 ) {

            this._user = user;
            this._startDate = startDate;
            this._endDate = endDate;
            this._costPerHour = costPerHour;
            this._workingHoursPerDay = workingHoursPerDay;
        }
        else
            throw new IllegalArgumentException("Erros nos valores dos parâmetros.");
    }

    public boolean equals( Object obj ) {

        if( this == obj )
            return true;

        if( obj instanceof Resource ) {
            Resource resource = (Resource) obj;

            boolean isUserEquals = this._user.equals(resource._user);
            if( isUserEquals && 
                this._startDate.equals(resource._startDate) && this._endDate.equals(resource._endDate) &&
                this._costPerHour == resource._costPerHour &&
                this._workingHoursPerDay == resource._workingHoursPerDay )
                return true;
        }
        return false;
    }

    public long maximumCostInPeriod( LocalDateTime startPeriod, LocalDateTime endPeriod ) throws IllegalArgumentException {
        long daysBetween = Duration.between(startPeriod, endPeriod).toDays();

        if( daysBetween >= 0)
            return daysBetween * this._workingHoursPerDay * this._costPerHour;
        else
            throw new IllegalArgumentException("Período inválido");
    }
}
