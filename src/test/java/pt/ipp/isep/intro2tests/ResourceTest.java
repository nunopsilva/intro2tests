package pt.ipp.isep.intro2tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeAll;

/**
 * Unit test for Resource.
 */
public class ResourceTest 
{
    static LocalDateTime startDate;
    static LocalDateTime endDate;

    static long workingHoursPerDay;
    static long costPerHour;

    @BeforeAll
    static public void setup() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd MM yyyy HH:mm");
        String inputStartDateString = "23 04 2021 00:00";
        String inputEndDateString = "30 04 2021 23:59";
        
        startDate = LocalDateTime.parse(inputStartDateString, dtf);
        endDate = LocalDateTime.parse(inputEndDateString, dtf);

        workingHoursPerDay = 8L;
        costPerHour = 10L;
    } 

    @Test
    public void shouldCreateAValidResource() throws Exception
    {
        // because no user's method will called, a real user object can be used, but a double could be used too.
        User lia = new User("Lia");

        new Resource( lia, startDate, endDate, costPerHour, workingHoursPerDay );
    }

    @Test
    public void shouldThrowException_becausePeriodIsInvalid()
    {
        // because no user's method will called, a double can be used.
        User userDouble = mock(User.class);

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            new Resource( userDouble, endDate, startDate, costPerHour, workingHoursPerDay );
        });
    
        String expectedMessage = "Erros nos valores dos parâmetros.";
        String actualMessage = exception.getMessage();
    
        assertTrue(actualMessage.contains(expectedMessage));
    }

    // faltam vários testes ao construtor

    @Test
    public void shouldReturnTrueEqualsSameResources() {

        User lia = new User("Lia");

        Resource resource = new Resource( lia, startDate, endDate, costPerHour, workingHoursPerDay );

        boolean isEquals = resource.equals(resource);

        assertTrue( isEquals );
    }

    @Test
    public void shouldReturnTrueEquals_allSameValues() {

        User lia = new User("Lia");

        Resource resource = new Resource( lia, startDate, endDate, costPerHour, workingHoursPerDay );
        Resource resource2 = new Resource( lia, startDate, endDate, costPerHour, workingHoursPerDay );

        boolean isEquals = resource.equals(resource2);

        assertTrue( isEquals );
    }

    @Test
    public void shouldReturnTrueEquals_allSameValuesExceptEqualUser() {

        User lia = new User("Lia");
        User lia2 = new User("Lia");

        Resource resource = new Resource( lia, startDate, endDate, costPerHour, workingHoursPerDay );
        Resource resource2 = new Resource( lia2, startDate, endDate, costPerHour, workingHoursPerDay );

        boolean isEquals = resource.equals(resource2);

        assertTrue( isEquals );
    }

    @Test
    public void shouldReturnFalseEquals_allSameValuesExceptDoubleUser() {

        User lia = new User("Lia");
        User lia2 = mock(User.class);

        Resource resource = new Resource( lia, startDate, endDate, costPerHour, workingHoursPerDay );
        Resource resource2 = new Resource( lia2, startDate, endDate, costPerHour, workingHoursPerDay );

        boolean isEquals = resource.equals(resource2);

        assertFalse( isEquals );
    }

    @Test
    public void shouldReturnTrueEquals_allSameValuesExceptDoubleUser() {

        User lia = new User("Lia");
        User lia2 = mock(User.class);

        when( lia.equals(lia2)).thenReturn( true );

        Resource resource = new Resource( lia, startDate, endDate, costPerHour, workingHoursPerDay );
        Resource resource2 = new Resource( lia2, startDate, endDate, costPerHour, workingHoursPerDay );

        boolean isEquals = resource.equals(resource2);

        assertFalse( isEquals );
    }

    @Test
    public void shouldReturnMaxCost() throws Exception
    {
        User userDouble = mock(User.class);

        Resource r = new Resource( userDouble, startDate, endDate, costPerHour, workingHoursPerDay );

        long maxCost = r.maximumCostInPeriod(startDate, endDate);

        long qtyOfDays = Duration.between( startDate, endDate ).toDays();

        assertEquals( maxCost, qtyOfDays * workingHoursPerDay * costPerHour );
    }


    @Test
    public void shouldThrowException_periodIsInvalid() throws IllegalArgumentException
    {
        User userDouble = mock(User.class);

        Resource r = new Resource( userDouble, startDate, endDate, costPerHour, workingHoursPerDay );

        Exception exception = assertThrows(Exception.class, () -> {
            r.maximumCostInPeriod(endDate, startDate);
        });
    
        String expectedMessage = "Período inválido";
        String actualMessage = exception.getMessage();
    
        assertTrue(actualMessage.contains(expectedMessage));
    }
}