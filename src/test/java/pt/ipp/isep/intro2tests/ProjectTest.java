package pt.ipp.isep.intro2tests;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeAll;

/**
 * Unit test for Project.
 */
public class ProjectTest 
{
    static LocalDateTime startDate;
    static LocalDateTime endDate;

    @BeforeAll
    static public void setup() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd MM yyyy HH:mm");
        String inputStartDateString = "23 04 2021 00:00";
        String inputEndDateString = "30 04 2021 23:59";
        
        startDate = LocalDateTime.parse(inputStartDateString, dtf);
        endDate = LocalDateTime.parse(inputEndDateString, dtf);
    } 

    @Test
    public void shouldCreateAValidResource() throws Exception
    {
        Resource r1 = mock(Resource.class);
        Resource r2 = mock(Resource.class);

        when(r1.maximumCostInPeriod(startDate, endDate)).thenReturn(5L);
        when(r2.maximumCostInPeriod(startDate, endDate)).thenReturn(15L);

        Project p = new Project( "título", startDate, endDate );
        p.addResource(r1);
        p.addResource(r2);

        long maxCost = p.maxCostInPeriod(startDate, endDate);

        assertEquals( maxCost, 20L);
    }
}