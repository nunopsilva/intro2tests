package pt.ipp.isep.intro2tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;

import org.junit.Test;

/**
 * Unit test for User.
 */
public class UserTest 
{
    @Test
    public void shouldCreateAValidUser() throws Exception
    {
        new User("Lia");
    }

    @Test(expected = Exception.class)
    public void shouldThrowExceptionWithEmptyName_inJUnit4() throws Exception
    {
        new User("");
    }

    @Test(expected = Exception.class)
    public void shouldThrowExceptionWithSingleSpaceName_inJUnit4() throws Exception
    {
        new User(" ");
    }

    @Test(expected = Exception.class)
    public void shouldThrowExceptionWithMultipleSpacesName_inJUnit4() throws Exception
    {
        new User("   ");
    }

    @Test(expected = Exception.class)
    public void shouldThrowExceptionWithTabName_inJUnit4() throws Exception
    {
        new User("\t");
    }

    @Test
    public void shouldThrowExceptionWithEmptyName_inJUnit5() throws Exception
    {
        Exception exception = assertThrows(Exception.class, () -> {
            new User("");
        });
    
        String expectedMessage = "Valor de parâmetro 'name' deve ser uma string não vazia.";
        String actualMessage = exception.getMessage();
    
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void shouldReturnTrueEqualsWithSameObject() {
        User lia = new User("Lia");

        boolean isEquals = lia.equals(lia);

        assertTrue( isEquals );
    }

    @Test
    public void shouldReturnTrueEqualsUsersWithSameName() {
        User lia = new User("Lia");

        User lia2 = new User("Lia");

        boolean isEquals = lia.equals(lia2);

        assertTrue( isEquals );
    }

    @Test
    public void shouldReturnFalseEqualsUsersWithDifNames() {
        User lia = new User("Lia");

        User lia2 = new User("Lia2");

        boolean isEquals = lia.equals(lia2);

        assertFalse( isEquals );
    }

    @Test
    public void shouldReturnFalseDifferentClasses() {
        User lia = new User("Lia");

        Object obj = new Object();

        boolean isEquals = lia.equals(obj);

        assertFalse( isEquals );
    }

    @Test
    public void shouldReturnFalse_becauseUserDoubleHasNullName() {
        User lia = new User("Lia");

        User userDouble = mock(User.class);

        boolean isEquals = lia.equals(userDouble);

        assertFalse( isEquals );
    }

    /*
    / este teste falha porque não é possível substituir equals()

    @Test
    public void shouldReturnFalse_becauseUserDoubleEqualsIsNotStubed() {
        User lia = new User("Lia");

        User userDouble = mock(User.class);
        when( userDouble.equals( lia )).thenReturn(true);

        boolean isEquals = userDouble.equals(lia);

        assertTrue( isEquals );
    }
    */
}
