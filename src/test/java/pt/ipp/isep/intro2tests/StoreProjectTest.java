package pt.ipp.isep.intro2tests;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeAll;

import org.mockito.ArgumentMatchers;

public class StoreProjectTest {
    
    static LocalDateTime startDate;
    static LocalDateTime endDate;

    @BeforeAll
    static public void setup() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd MM yyyy HH:mm");
        String inputStartDateString = "23 04 2021 00:00";
        String inputEndDateString = "30 04 2021 23:59";

        startDate = LocalDateTime.parse(inputStartDateString, dtf);
        endDate = LocalDateTime.parse(inputEndDateString, dtf);
    }

    @Test
    public void shouldCreateAndAddProject() throws Exception
    {
        // Arrange
        String titulo = "Título";

        Project projectDouble = mock( Project.class );

        ProjectFactory projectFactoryDouble =  mock( ProjectFactory.class );
        when(projectFactoryDouble.createProject(anyString(), ArgumentMatchers.any(LocalDateTime.class), ArgumentMatchers.any(LocalDateTime.class)) ).thenReturn( projectDouble );

        StoreProjectReeng sp = new StoreProjectReeng( projectFactoryDouble );
        boolean hasCreated = sp.createAndAddProject( titulo, startDate, endDate );

        assertTrue( hasCreated );
    }

    @Test
    public void shouldNotCreateAndAddRepeatedTitleProject() throws Exception
    {
        // Arrange
        String titulo = "Título";

        Project projectDouble = mock( Project.class );
        when( projectDouble.hasTitle( titulo ) ).thenReturn( true );

        ProjectFactory projectFactoryDouble =  mock( ProjectFactory.class );
        when(projectFactoryDouble.createProject(titulo, startDate, endDate) ).thenReturn( projectDouble );

        StoreProjectReeng storeProjectReeng = new StoreProjectReeng( projectFactoryDouble );

        // should work fine
        boolean hasCreated = storeProjectReeng.createAndAddProject( titulo, startDate, endDate );

        // Act + Assert
        // throws IllegalArgumentException, because repeated title
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            storeProjectReeng.createAndAddProject( titulo, startDate, endDate );
        });
    
        String expectedMessage = "Título já existente.";
        String actualMessage = exception.getMessage();
    
        assertTrue(actualMessage.contains(expectedMessage));
    }
}
